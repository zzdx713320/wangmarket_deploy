## 介绍
网市场云建站系统部署到线上服务器，就是部署的这个，wangmarket本身是以jar形式存在，以便后续升级  
这种部署方式会牵扯到linux服务器，以及执行一些命令行进行安装，当然命令行您直接复制粘贴就行。但如果您之前从未接触过的话，可能也会很吃力，针对这种情况我们也有另一个已纳入华为开发组织的无服务器无门槛的版本，windows系统直接下载双击就能使用，也不用买服务器   https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms

# 线上部署

#### 1. 服务器系统：CentOS 7.4  
如果没有7.4，请选7.6。  
7.6也没有，7.0~7.6之间随便选个试试吧。
其他版本只是猜测应该没问题。只是试过7.4的。

#### 2. 一键安装部署

````
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/shell/normal_install.sh -O install.sh && chmod -R 777 install.sh && sh ./install.sh
````
我们在华为云、阿里云服务器都试过没问题，如果您安装时不成功，可以联系我们

#### 3. 使用
安装成功后，启动可能需要半分钟，等半分钟，访问 /login.do 按照安装步骤提示及说明进行操作

## 扩展：本地编译，更新到服务器，适用于二次开发    
你可以直接将本项目拉到你本地eclipse中进行编译运行，eclipse不知如何导入，可参考 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3600882&doc_id=1101390  
将生成的运行文件上传本项目到 /mnt/tomcat8/webapps/ROOT/ 下 

## 版本更新
直接更新 WEB-INF/lib/wangmarket-xxx.jar 即可完成更新 (有时还会更新其他jar包，到时详细查阅 [更新说明](http://www.wang.market/log.html) 里  需要更新的jar包即可。)

# 二次开发及扩展

#### 快速吧项目跑起来，建立一个网站
1. [本地环境及git导入项目](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5808068)
2. [配置Mysql数据库（可选，不做这一步也完全能运行起来）](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5810398)
3. [代码起来后，通过安装引导，安装系统](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5808141)
4. [用三分钟的时间，导入一个模板快速创建一个自己的网站](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5808118)

### 原本的页面及功能修改、重写
比如，感觉原本的登录页面不好看，那么可以对登录页进行重写。重写方式参考： https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3631374&doc_id=1101390  
wangmarket 的源码文件在项目：  https://gitee.com/mail_osc/wangmarket  

### 定制开发自己想要的功能
参考：  https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3213258&doc_id=1101390